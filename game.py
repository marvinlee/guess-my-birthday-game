from random import randint

name = input("Hi! What is your name?")
guess_number = 1
num_guesses = 5

for guesses in range(num_guesses):
  guess_month = randint(1, 12)
  guess_year = randint(1924, 2004)
  print("Guess", guess_number, ":", name, "were you born on", guess_month, "/", guess_year, "?")
  answer = input("yes or no?")

  if answer == "yes":
    print("I knew it!")
    exit()
  elif guesses < 4:
    print("Drat! Lemme try again")
    guess_number = guess_number + 1
  else:
    print("I have other things to do. Good bye.")
